import {url} from '../config.js'

let posts = document.querySelector('#posts')
let comments = document.querySelector('#comments')
let albums = document.querySelector('#albums')
let photos = document.querySelector('#photos')
let todos = document.querySelector('#todos')
let users = document.querySelector('#users')


//Posts function
function fill_posts_in_main(json){
    let main = document.querySelector('.main')
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < obj.length; i++) {
        let div = document.createElement("div");
        div.innerHTML = 'UserID: ' + obj[i].userId 
        + '<br>' + 'Id: ' + obj[i].id
        + '<br>' + 'Title: ' + obj[i].title
        + '<br>' + 'Body: ' + obj[i].body 
        div.style.border = 'thin solid black'
        div.style.padding = '20px'
        main.appendChild(div);
    }
    window.history.replaceState(null, 
       null, "/posts");
}
posts.addEventListener('click', function(){
    fetch(url + 'posts')
    .then(response => response.json())
    .then(json => fill_posts_in_main(json))
})


//Comments function
function fill_comments_in_main(json){
    let main = document.querySelector('.main')
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < obj.length; i++) {
        let div = document.createElement("div");
        div.innerHTML = 'PostID: ' + obj[i].postId 
        + '<br>' + 'Id: ' + obj[i].id
        + '<br>' + 'Name: ' + obj[i].name
        + '<br>' + 'Email: ' + obj[i].email
        + '<br>' + 'Body: ' + obj[i].body
        div.style.border = 'thin solid black'
        div.style.padding = '20px'
        main.appendChild(div);
    }
    window.history.replaceState(null, 
        null, "/comments");
}
comments.addEventListener('click', function(){
    fetch(url + 'comments')
    .then(response => response.json())
    .then(json => fill_comments_in_main(json))
})


//Albums function
function fill_albums_in_main(json){
    let main = document.querySelector('.main')
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < obj.length; i++) {
        let div = document.createElement("div");
        div.innerHTML = 'UserID: ' + obj[i].userId 
        + '<br>' + 'Id: ' + obj[i].id
        + '<br>' + 'Title: ' + obj[i].title
        div.style.border = 'thin solid black'
        div.style.padding = '20px'
        div.style.flex = '50%'
        main.appendChild(div);
    }
    window.history.replaceState(null, 
        null, "/albums");
}
albums.addEventListener('click', function(){
    fetch(url + 'albums')
    .then(response => response.json())
    .then(json => fill_albums_in_main(json))
})


//Photos function
function fill_photos_in_main(json){
    let main = document.querySelector(".main");
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < 50; i++) {
        let div = document.createElement("div");
        div.innerHTML = `<img src= ${obj[i].thumbnailUrl}></img>`
        + '<br><br>' + 'UserID: ' + obj[i].albumId 
        + '<br>' + 'Id: ' + obj[i].id
        + '<br>' + 'Title: ' + obj[i].title
        div.style.flex = '50%'
        div.style.border = '1px solid gray'
        div.style.padding = '10px'
        main.appendChild(div)
    }
    window.history.replaceState(null, 
    null, "/photos");
}
photos.addEventListener('click', function(){
    fetch(url + 'photos')
    .then(response => response.json())
    .then(json => fill_photos_in_main(json))
})


//Todos function
function fill_todos_in_main(json){
    let main = document.querySelector('.main')
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < obj.length; i++) {
        let div = document.createElement("div");
        div.innerHTML = 'UserID: ' + obj[i].userId 
        + '<br>' + 'Id: ' + obj[i].id
        + '<br>' + 'Title: ' + obj[i].title
        + '<br>' + 'Completed: ' + obj[i].completed
        div.style.border = 'thin solid black'
        div.style.padding = '20px'
        div.style.flex = '50%'
        main.appendChild(div);
    }
    window.history.replaceState(null, 
    null, "/todos");
}
todos.addEventListener('click', function(){
    fetch(url + 'todos')
    .then(response => response.json())
    .then(json => fill_todos_in_main(json))
})


//Users function
function fill_users_in_main(json){
    let main = document.querySelector('.main')
    let stringify = JSON.stringify(json)
    let obj = JSON.parse(stringify)
    main.innerText = ''
    for (let i = 0; i < obj.length; i++) {
        let div = document.createElement("div");
        div.innerHTML = 'Id: ' + obj[i].id
        + '<br>' + 'Name: ' + obj[i].name
        + '<br>' + 'Username: ' + obj[i].username
        + '<br>' + 'Email: ' + obj[i].email
        + '<br>' + 'Street: ' + obj[i].address.street
        + '<br>' + 'Suite: ' + obj[i].address.suite
        + '<br>' + 'City: ' + obj[i].address.city
        + '<br>' + 'Zip Code: ' + obj[i].address.zipcode
        + '<br>' + 'Latitude: ' + obj[i].address.geo.lat
        + '<br>' + 'Longitude: ' + obj[i].address.geo.lng
        + '<br>' + 'Phone: ' + obj[i].phone
        + '<br>' + 'Website: ' + obj[i].website
        + '<br>' + 'Company name: ' + obj[i].company.name
        + '<br>' + 'CatchPhrase: ' + obj[i].company.catchPhrase
        + '<br>' + 'BS: ' + obj[i].company.bs
        div.style.border = 'thin solid black'
        div.style.padding = '20px'
        div.style.flex = '100%'
        main.appendChild(div);
    }
    window.history.replaceState(null, 
    null, "/users");
}
users.addEventListener('click', function(){
    fetch(url + 'users')
    .then(response => response.json())
    .then(json => fill_users_in_main(json))
})
