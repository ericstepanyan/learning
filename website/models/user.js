export default class User {

    constructor(name, email, age) {
        this.name = name;
        this.email = email;
        this.age = age;    
    }

    create() {

        return `insert into users (Name,age,email) values ${this.values.map(e => '("' + e.name + '",' + e.age + ',' + '"' + e.email + '")').join(",")}`
        

    }

    read() {
        if (this.id){
            return `select * from users where Id = ${this.id}`
        }else{
            return `select * from users`
        }
    }

    update() {
        return `update users set Name = '${this.name}', age = '${this.age}', email = '${this.email}' where id = '${this.id}'`
    }

    delete() {
        
        return `delete from users where Id in (${this.values.map(e => e.id).join(",")})`

    }

    hash_password() {

    }

    validate_email() {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.email)
        
    }

    validate_username() {

    }

}
