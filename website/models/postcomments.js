export default class PostComment {

    constructor(id, comment, post_id, user_id) {
        this.id = id
        this.comment = comment
        this.post_id = post_id
        this.user_id = user_id
    }

    create() {
        return `insert into postcomments values (default, ${this.post_id}, ${this.user_id}, '${this.comment}' )`

    }

    read() {
        return `select * from postcomments where Posts_PostId = ${this.post_id}`
    }

    update() {
        return `update postcomments set Comment = '${this.comment}' where  Users_Id = ${this.post_id}`
    }

    delete() {
        return `delete from postcomments where Id = ${this.id}`
    }



}
