export default class Album {

    constructor(id, user_id, title) {
        this.id = id;
        this.user_id = user_id;
        this.title = title;
            
    }

    create() {
    
        return `insert into albums values (default, ${this.user_id}, '${this.title}')`
    }

    read() {
        if (this.user_id){
            return `select * from albums where Users_Id = ${this.user_id}`
        }else{
            return `select * from albums`
        }
    }

    update() {
        return `update albums set title = '${this.title}' where Id = ${this.id} and Users_Id = ${this.user_id}`
    }

    delete() {

        return `delete from albums where Id in (${this.values.map(e => e.id).join(",")})`

    }

}
