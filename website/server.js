import mysql from "mysql";
import express from "express";
import { appPort as port, dbConfig } from "./config.js";
import * as user from "./controllers/user.js";
import * as post from "./controllers/post.js";
import * as postcomment from "./controllers/postcomments.js";
import * as album from "./controllers/album.js";

// Create express app
const app = express()
// IMport morgan logger
const morgan = require('morgan')
const path = require('path')
const fs = require('fs')

const accessLogStream = fs.createWriteStream(path.join(__dirname, './logs/log.txt'), { flags: 'a' })

// apply json middlware for request objects
app.use(express.json());
app.use(morgan(':date[web] METHOD: :method URL: :url Status Code: :status Response time: :response-time ms Response length: :res[content-length]', { stream: accessLogStream }))


//Create database connection
const dbConnection = mysql.createConnection(dbConfig);

export async function dbQuery(query) {
  /*
    Function for making query to db
    @query: sql query
    return: promise object
  */
  return new Promise((resolve, reject) => {
    dbConnection.query(query, (error, results, fields) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}


// user router
app.get("/users", user.getUser);
app.get("/users/:userId", user.getUser);
app.post("/users", user.creatUser);
app.post("/users-bulk-create", user.bulkCreateUser);
app.delete("/users/:userId", user.deleteUser);
app.delete("/users", user.bulkDeleteUser);
app.patch("/users/:userId", user.updateUser);
app.put("/users/:userId", user.updateUser);
// use test route to view your request body
app.post('/test', (req, res) => {
  res.json({requestBody: req.body})
})
//post router
app.get("/posts", post.getPost);
app.post("/posts", post.createPost);
app.put("/posts", post.updatePost);
app.delete("/posts", post.deletePost);
app.delete("/posts-bulk-delete", post.bulkDeletePost);

//postcomment router
app.get("/postcomments", postcomment.getPostComment);
app.post("/postcomments", postcomment.createPostComment);
app.put("/postcomments", postcomment.updatePostComment);
app.delete("/postcomments", postcomment.deletePostComment);


//album router
app.get("/albums", album.getAlbum);
app.post("/albums", album.createAlbum);
app.put("/albums", album.updateAlbum);
app.delete("/albums", album.deleteAlbum);
app.delete("/albums-bulk-delete", album.bulkDeleteAlbum);


app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
