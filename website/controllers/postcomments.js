import PostComment from "../models/postcomments";
import {dbQuery} from "../server.js";



  export async function getPostComment(req, res) {
    let comment = new PostComment()
    comment.post_id = req.body.post_id
    try {
        const data = await dbQuery(comment.read());
        if (Object.keys(data).length === 0){
          res.sendStatus(404)
        }else{
          res.status(200).send(JSON.stringify(data))
        }
    } catch (error) {
      console.log(error, "ERROR");
    }
  }


  export async function createPostComment(req, res) {
    let comment = new PostComment()
    comment.post_id = req.body.post_id
    comment.user_id = req.body.user_id
    comment.comment = req.body.comment
    try {
      const data = await dbQuery(comment.create())
      if (data.affectedRows == 0){
        res.sendStatus(400)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
    }
  }

  export async function deletePostComment(req, res) {
    let comment = new PostComment()
    comment.id = req.body.id

    try {
      const data = await dbQuery(comment.delete())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
    }
  
  }


  export async function updatePostComment(req, res) {
    let comment = new PostComment()
    comment.comment = req.body.comment
    comment.post_id = req.body.post_id
    try {
      const data = await dbQuery(comment.update())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
      
    }
  }
