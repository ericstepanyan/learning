import Post from "../models/post";
import {dbQuery} from "../server.js";



  export async function getPost(req, res) {
    let post = new Post()
    
    post.user_id = req.body.user_id
    try {
        const data = await dbQuery(post.read());
        if (Object.keys(data).length === 0){
          res.sendStatus(404)
        }else{
          res.status(200).send(JSON.stringify(data))
        }
    } catch (error) {
      console.log(error, "ERROR");
    }
  }


  export async function createPost(req, res) {
    let post = new Post()
    post.text = req.body.text
    post.user_id = req.body.user_id
    try {
      const data = await dbQuery(post.create())
      if (data.affectedRows == 0){
        res.sendStatus(400)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
    }
  }

  export async function deletePost(req, res) {
    let post = new Post()
    post.id = req.body.id
      try {
        const data = await dbQuery(post.delete())
        if (data.affectedRows == 0){
          res.sendStatus(404)
        }else{
          res.sendStatus(200)
        }
      } catch (error){
        console.log(error, "ERROR");
      }   
  }

  export async function bulkDeletePost(req, res) {
    let post = new Post()
      post.values = req.body
      try {
        const data = await dbQuery(post.delete())
        if (data.affectedRows == 0){
          res.sendStatus(404)
        }else{
          res.sendStatus(200)
        }
      } catch (error){
        console.log(error, "ERROR");
      }    
  }

  export async function updatePost(req, res) {
    let post = new Post()
    post.id = req.body.id
    post.text = req.body.text
    post.user_id = req.body.user_id
    try {
      const data = await dbQuery(post.update())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
      
    }
  }
