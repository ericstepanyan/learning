import User from "./../models/user";
import {dbQuery} from "../server.js";



  export async function getUser(req, res) {
    let user = new User()
    user.id = req.params.userId
    try {
        const data = await dbQuery(user.read());
        if (Object.keys(data).length === 0){
          res.sendStatus(404)
        }else{
          res.status(200).send(JSON.stringify(data))
        }
    } catch (error) {
      console.log(error, "ERROR");
    }
  }

 export async function bulkCreateUser(req, res) {
   let user = new User()
   user.values = req.body
    try {
        const data = await dbQuery(user.create());
        if (data.affectedRows == 0){
          res.sendStatus(400)
        }else{
          res.sendStatus(200)
        }
      } catch (error) {
        console.log(error, "ERROR");
      }
  }

  export async function creatUser(req, res) {
    let user = new User()
    user.name = req.body.name
    user.age = req.body.age
    user.email = req.body.email
      try {
          if(user.validate_email() == false){
            res.status(400).send("Invalid Email")
            return
          }
          const data = await dbQuery(user.create())
          if (data.affectedRows == 0){
            res.sendStatus(400)
          }else{
            res.sendStatus(200)
          }
      } catch (error){
        console.log(error, "ERROR");
      }
  }

  export async function deleteUser(req, res) {
    let user = new User()
    user.id = req.params.userId
    try {
      const data = await dbQuery(user.delete())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
    }
  
  }

  export async function bulkDeleteUser(req, res) {
    let user = new User()
    user.values = req.body
      try {
        const data = await dbQuery(user.delete())
        if (data.affectedRows == 0){
          res.sendStatus(400)
        }else{
          res.sendStatus(200)
        }
      } catch (error){
        console.log(error, "ERROR");
      }  
  }

  export async function updateUser(req, res) {
    let user = new User()
    user.id = req.params.userId
    user.name = req.body.name
    user.age = req.body.age
    user.email = req.body.email

    try {
      const data = await dbQuery(user.update())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
      
    }
  }
