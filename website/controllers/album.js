import Album from "../models/album";
import {dbQuery} from "../server.js";



  export async function getAlbum(req, res) {
    let album = new Album()
    
    album.user_id = req.body.user_id
    try {
        const data = await dbQuery(album.read());
        if (Object.keys(data).length === 0){
          res.sendStatus(404)
        }else{
          res.status(200).send(JSON.stringify(data))
        }
    } catch (error) {
      console.log(error, "ERROR");
    }
  }


  export async function createAlbum(req, res) {
    let album = new Album()
    album.title = req.body.title
    album.user_id = req.body.user_id
    try {
      const data = await dbQuery(album.create())
      res.sendStatus(200)
    } catch (error){
      res.sendStatus(400)
      console.log(error, "ERROR");
    }
  }

  export async function deleteAlbum(req, res) {
    let album = new Album()
    album.id = req.body.id
      try {
        const data = await dbQuery(album.delete())
        if (data.affectedRows == 0){
          res.sendStatus(404)
        }else{
          res.sendStatus(200)
        }
      } catch (error){
        console.log(error, "ERROR");
      }   
  }

  export async function bulkDeleteAlbum(req, res) {
    let album = new Album()
      album.values = req.body
      try {
        const data = await dbQuery(album.delete())
        if (data.affectedRows == 0){
          res.sendStatus(404)
        }else{
          res.sendStatus(200)
        }
      } catch (error){
        console.log(error, "ERROR");
      }  
  }

  export async function updateAlbum(req, res) {
    let album = new Album()
    album.id = req.body.id
    album.text = req.body.text
    album.user_id = req.body.user_id
    try {
      const data = await dbQuery(album.update())
      if (data.affectedRows == 0){
        res.sendStatus(404)
      }else{
        res.sendStatus(200)
      }
    } catch (error){
      console.log(error, "ERROR");
      
    }
  }
