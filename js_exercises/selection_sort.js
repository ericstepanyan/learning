
function selectionSort(arr){
    for(let splitter = 0; splitter < arr.length - 1; splitter++){
        
        let indexSmall = splitter

        for(let i = splitter + 1; i < arr.length; i++){
            if(arr[indexSmall] > arr[i]){
                indexSmall = i
            }
        }
        let temp = arr[splitter]
        arr[splitter] = arr[indexSmall]
        arr[indexSmall] = temp  
    }
}


let arr = [10,15,23,2,3,28,1,36]
selectionSort(arr)
console.log(arr)