let arr = [3, 'a', 2, 'a','a','a',5,9,3,3]

function calc(arr){
    let occurs = {}
    let curItem
    let maxNumber = arr[0]

    for (let i = 0; i < arr.length; i++){
        curItem = arr[i]

        if(occurs[curItem] !== undefined){
            occurs[curItem]++
        }else{
            occurs[curItem] = 1
        }

        if(occurs[curItem] > occurs[maxNumber]){
            maxNumber = curItem
        }
    }
    let key = Object.keys(occurs)[Object.values(occurs).indexOf(occurs[maxNumber])]
    return key + ', ' + occurs[maxNumber] + ' times'
}

console.log(calc(arr))