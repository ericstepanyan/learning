let cache = {}

function fibonacci(n) {
    if (n <= 1){ 
        return n
    }
    if (n in cache){
        return cache[n]
    }
    let value = fibonacci(n-1) + fibonacci(n-2)

    cache[n] = value

    return value
}

console.log(fibonacci(6))
