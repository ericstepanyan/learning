let arr1 = [1, 3, 6, 7]
let arr2 = [2, 4, 5]

function merge(arr1, arr2) {
    let fin_arr = []
    let i = 0
    let j = 0
    let index = 0

    while(index != (arr1.length + arr2.length)){
        
        let isArr1Over = i >= arr1.length
        let isArr2Over = j >= arr2.length

        if(!isArr1Over && (isArr2Over || (arr1[i] < arr2[j]))){
            fin_arr[index] = (arr1[i])
            i++
        } else {
            fin_arr[index] = (arr2[j])
            j++
        }
        index++
    }
    return fin_arr
}

console.log(merge(arr1, arr2))